import { Injectable } from '@angular/core';
import { StorageKeys } from 'src/app/enums/storage-key.enum';
import { getStorage } from 'src/app/utils/storage.utils';
import { SessionStorageModel } from '../../models/session-storage.model';
@Injectable({
    providedIn: 'root'
})

export class SessionStorageService {
    SessionStorageModel:SessionStorageModel = new SessionStorageModel();
    constructor(){}

    public set(key:string, value:string){
        this.SessionStorageModel[key] = value;
    }
    
    active(): boolean {
        const trainer = getStorage(StorageKeys.TRAINER)
        return Boolean(trainer)
    }
    
    get(key:string) : string {
        return this.SessionStorageModel[key]
    }

    remove(key:string) : void {
        this.SessionStorageModel[key] = null;
    }

    clear() : void {
        this.SessionStorageModel = new SessionStorageModel();
    }
    
}