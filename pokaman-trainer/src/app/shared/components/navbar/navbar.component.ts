import { Component, OnChanges, OnInit } from '@angular/core';
import { StorageKeys } from 'src/app/enums/storage-key.enum';
import { LoginService } from 'src/app/features/login/services/login.service';
import { SessionStorageModel } from 'src/app/models/session-storage.model';
import { getStorage } from 'src/app/utils/storage.utils';
import { SessionStorageService } from '../../services/session-storage.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  trainerName: string = null;
  storageJson: any =  {};

  constructor(private readonly sessionService: SessionStorageService, private loginService: LoginService) {
    loginService.getLoggedInName.subscribe(trainerName => this.trainerName = trainerName);
  }
  ngOnInit(): void {
      if(getStorage(StorageKeys.TRAINER) != null) {
        this.storageJson = getStorage(StorageKeys.TRAINER);
        this.trainerName = this.storageJson.name;
      }
  }

  get hasActiveSession(): boolean {
    return this.sessionService.active();
  }

  onLogOutClick(): void {
    this.trainerName = null;
    localStorage.removeItem(StorageKeys.TRAINER);
  }
}
