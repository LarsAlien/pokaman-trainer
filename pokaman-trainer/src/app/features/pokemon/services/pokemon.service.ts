import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, shareReplay } from 'rxjs/operators';
import { PokemonResponse } from 'src/app/models/pokemon-response.model';
import { Pokemon } from 'src/app/models/pokemon.model';
import { getMasterImageUrl } from 'src/app/utils/pokemon-image.util';
import { environment } from 'src/environments/environment';

const { pokeAPI } = environment;

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  private readonly pokemonCache$;
  public pokemon: Pokemon[] = [];
  public error: string = '';
  constructor(private readonly http: HttpClient) {
    this.pokemonCache$ = this.http
      .get<PokemonResponse>(`${pokeAPI}/pokemon?limit=100`)
      .pipe(shareReplay(1));
  }

  fetchPokemon(): void {
    this.pokemonCache$
      .pipe(
        map((response: PokemonResponse) => {
          return response.results.map((pokemon) => ({
            ...pokemon,
            ...this.getIdAndImage(pokemon.url),
          }));
        })
      )
      .subscribe(
        (pokemon: Pokemon[]) => {
          this.pokemon = pokemon;
        },
        (errorResponse: HttpErrorResponse) => {
          this.error = errorResponse.message;
        }
      );
  }

 

  private getIdAndImage(url: string): any {
    const id = Number(url.split('/').filter(Boolean).pop());
    return {
      id,
      image: getMasterImageUrl(id),
    };
  }
}
