import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from '../services/pokemon.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon.container.html',
  styleUrls: ['./pokemon.container.scss']
})
export class PokemonContainer implements OnInit {

    constructor(private readonly pokemonService: PokemonService) {

    }

    get pokemon(): Pokemon[] {
        return this.pokemonService.pokemon;
    }

   

  ngOnInit(): void {
      this.pokemonService.fetchPokemon()
  }
}
