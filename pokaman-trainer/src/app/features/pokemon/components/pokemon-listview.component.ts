import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
@Component({
  selector: 'app-pokemon-listview',
  templateUrl: './pokemon-listview.component.html',
  styleUrls: ['./pokemon-listview.component.scss']
})
export class PokemonListViewComponent {
    constructor( private readonly router: Router) {}
  @Input() pokemon: Pokemon[] = [];
  @Output() pokemonClickEvent = new EventEmitter<string>();


  

  handlePokemonClick = (pokemonName: string) => {
    this.router.navigateByUrl( `/pokemon/${pokemonName}` );
  }
}
