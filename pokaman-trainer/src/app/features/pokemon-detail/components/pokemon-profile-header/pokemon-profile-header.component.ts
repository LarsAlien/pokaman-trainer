import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
@Component({
  selector: 'app-pokemon-profile-header',
  templateUrl: './pokemon-profile-header.html',
  styleUrls: ['./pokemon-profile-header.scss']
})
export class PokemonProfileHeader {
  @Input() pokemon: Pokemon;
  @Output() catchPokemonEvent = new EventEmitter<Pokemon>();

  handleCatchClick(): void {
    //   alert(`You caught a ${this.pokemon.name}`)
    this.pokemon.owned = true;
    this.catchPokemonEvent.emit(this.pokemon);
  }
}
