import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonDetailService } from '../services/pokemon-detail.service';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.container.html',
  styleUrls: ['./pokemon-detail.container.scss']
})
export class PokemonDetailContainer implements OnInit {

    private readonly pokemonName: string = '';

    constructor(
        private readonly route: ActivatedRoute,
        private readonly pokemonDetailService: PokemonDetailService) {
            this.pokemonName = this.route.snapshot.paramMap.get('id')
        }

    ngOnInit(): void {
          this.pokemonDetailService.fetchPokemonByName(this.pokemonName)
          
      }
    
    catchPokemonEvent(pokemon: Pokemon) {
        this.pokemonDetailService.addNewPokemon(pokemon);
    }

    get pokemon(): Pokemon {
        
        return this.pokemonDetailService.pokemon;
    }
    

  
}
