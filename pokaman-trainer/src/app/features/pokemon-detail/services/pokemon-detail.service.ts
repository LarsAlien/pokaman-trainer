import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, mergeMap, shareReplay, tap } from 'rxjs/operators';
import { StorageKeys } from 'src/app/enums/storage-key.enum';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { SessionStorageService } from 'src/app/shared/services/session-storage.service';
import { getMasterImageUrl } from 'src/app/utils/pokemon-image.util';
import { getStorage } from 'src/app/utils/storage.utils';
import { environment } from 'src/environments/environment';

const { pokeAPI, trainerAPI } = environment;

@Injectable({
  providedIn: 'root',
})
export class PokemonDetailService {
  public pokemon: Pokemon;

  public trainer: Trainer;
  storageJson: any = {};

  constructor(
    private readonly http: HttpClient,
    private readonly sessionStorage: SessionStorageService
  ) {
    this.storageJson = getStorage(StorageKeys.TRAINER);
  }

//   public fetchPokemonByName(name: string): void {
//     this.http
//       .get<Pokemon>(`${pokeAPI}/pokemon/${name}`)
//       .pipe(
//         map((pokemon: Pokemon) => ({
//           ...pokemon,
//           image: getMasterImageUrl(pokemon.id),
//         }))
//       )
//       .subscribe((pokemon: Pokemon) => {
//         this.pokemon = pokemon;
//       });
//   }

  public fetchPokemonByName(name: string): void {
    const getResponse = this.http
    .get<Trainer[]>(`${trainerAPI}/trainers?name=${this.storageJson.name}`)
    .pipe(
      map((trainers: Trainer[]) =>
        trainers.pop()
      )
    )
    .subscribe((trainer: Trainer) => {
        this.http
        .get<Pokemon>(`${pokeAPI}/pokemon/${name}`)
        .pipe(
          map((pokemon: Pokemon) => ({
            ...pokemon,
            image: getMasterImageUrl(pokemon.id),
            owned: trainer.pokemon.find(p => p.name === pokemon.name)
                    ? true : false
          }))
        )
        .subscribe((pokemon: Pokemon) => {
          this.pokemon = pokemon;
        });
      })
  }

    public addNewPokemon(pokemon: Pokemon): void {
        const getResponse = this.http
      .get<Trainer[]>(`${trainerAPI}/trainers?name=${this.storageJson.name}`)
      .pipe(
        map((trainers: Trainer[]) =>
          trainers.pop()
        )
      )
      .subscribe((trainer: Trainer) => {
        const patchResponse$ = this.http
        .patch(`${trainerAPI}/trainers/${trainer.id}`, {
            pokemon: [
                ...trainer.pokemon,
                {
                  name: pokemon.name,
                  url: `https://pokeapi.co/api/v2/pokemon/${pokemon.id}`
                }
            ]
        }).subscribe((res) => {console.log(res);
        })
      });
      
    }

  
}
