import { getStorage } from 'src/app/utils/storage.utils';
import { ActivatedRoute, Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Component } from '@angular/core';
import { SessionStorageService } from 'src/app/shared/services/session-storage.service';
import { TrainerService } from '../services/trainer.service';
import { StorageKeys } from 'src/app/enums/storage-key.enum';
import { Trainer } from 'src/app/models/trainer.model';
import { getMasterImageUrl } from 'src/app/utils/pokemon-image.util';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.container.html',
  styleUrls: ['./trainer.container.scss'],
})
export class TrainerContainer {
  
    

  constructor(
    private readonly sessionStorage: SessionStorageService,
    private readonly trainerService: TrainerService
  ) {
    this.storageJson = getStorage(StorageKeys.TRAINER)
    this.trainerName = this.storageJson.name;
  }

  

  ngOnInit(): void {
    this.trainerService.fetchTrainerPokemon(this.trainerName)
        .subscribe((pokemon: Pokemon[]) => this.pokemon = pokemon
        );
  }

  private getIdAndImage(url: string): any {
    const id = Number(url.split('/').filter(Boolean).pop());
    return {
      id,
      image: getMasterImageUrl(id),
    };
  }
  
  title = 'pokaman-trainer';
  trainerName: string = '';
  pokemon: Pokemon[];
  storageJson : any = {};
}
