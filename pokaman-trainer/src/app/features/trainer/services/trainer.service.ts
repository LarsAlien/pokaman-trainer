import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { getMasterImageUrl } from 'src/app/utils/pokemon-image.util';
import { environment } from 'src/environments/environment';

const { trainerAPI } = environment;
@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  public ownedPokemon: any[] = null;
  constructor(private http: HttpClient) {}

  public fetchTrainerPokemon(trainerName: string): Observable<any[]> {
    return this.http
      .get<Trainer[]>(`${trainerAPI}/trainers?name=${trainerName}`)
      .pipe(
        map((trainers: Trainer[]) =>
          trainers.pop().pokemon.map((pokemon) => ({
            ...pokemon,
            ...this.getIdAndImage(pokemon.url),
          }))
        )
      );
    //

    // .subscribe((trainers: any[]) => {
    //   console.log(trainers);

    // })

    // this.http
    //   .get(`${trainerAPI}/trainers`)
    //   .pipe(map((trainers: any[]) =>
    //     trainers.find(trainer => trainer.name === trainerName)))
    //   .pipe(map((trainer: Trainer) =>
    //     trainer.name ))
    //     .subscribe((trainer: Trainer) => {
    //       console.log(trainer);

    //     });
  }

  private getIdAndImage(url: string): any {
    const id = Number(url.split('/').filter(Boolean).pop());
    return {
      id,
      image: getMasterImageUrl(id),
    };
  }
}
