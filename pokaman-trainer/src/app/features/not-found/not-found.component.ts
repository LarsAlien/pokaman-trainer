import { Component } from "@angular/core";

@Component({
    selector: 'app-not-found',
    template: `
        <h1>404</h1>
        <p>Page not found</p>
    `,
    styles: [
        `
            .container {
                max-width: 75em;
                width: 100%;
                margin: 0 auto;
                padding: 0 1.5em;
            }
        `
    ]
})
export class NotFoundComponent{}


