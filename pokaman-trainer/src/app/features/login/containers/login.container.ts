import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from 'src/app/shared/services/session-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.container.html',
  styleUrls: ['./login.container.scss']
})
export class LoginContainer {
  
    constructor( private readonly router: Router, private readonly sessionStorage: SessionStorageService) {
        if(sessionStorage.active()) {
            this.router.navigateByUrl( '/pokemon' );
        }
    }
    

    
    handleLoginSuccess(): void {
        this.router.navigateByUrl( '/pokemon' );
    }
}
