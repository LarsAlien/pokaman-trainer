import { Component, EventEmitter, Output } from "@angular/core";
import { AbstractControl, FormControl, FormGroup, Validators } from "@angular/forms";
import { LoginService } from "../../services/login.service";

@Component({
    selector: 'app-login-form',
    templateUrl: './login-form.component.html',
    styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {

    @Output() success: EventEmitter<void> = new EventEmitter();

    loginForm: FormGroup = new FormGroup({
        trainerName: new FormControl('', [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(20)
        ])
    })

    constructor(private readonly loginService: LoginService) {

    }

    get trainerName(): AbstractControl {
        return this.loginForm.get('trainerName')
    }

    get loading(): boolean {
        return this.loginService.loading;
    }

    get error(): string {
        return this.loginService.error;
    }

    onLoginClick() {
        const { trainerName } = this.loginForm.value
        console.log(this.loginForm.value);
        
        this.loginService.login( this.loginForm.value.trainerName )
            .subscribe(
                this.handleSuccessfulLogin.bind(this)
            )
    }

    handleSuccessfulLogin(trainer): void {
        this.success.emit();
    }

    handleErrorLogin(error): void {
        if(this.loginService.errorCount >= 3) {
            //Block login for time
        }
        console.log(this.error);
    }
}