import { Trainer } from './../../../models/trainer.model';
import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, finalize, map, mergeMap, tap } from 'rxjs/operators';
import { StorageKeys } from 'src/app/enums/storage-key.enum';
import { setStorage } from 'src/app/utils/storage.utils';
import { environment } from 'src/environments/environment';

const { trainerAPI } = environment;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  @Output() getLoggedInName: EventEmitter<string> = new EventEmitter();
  public loading: boolean = false;
  public error: string = '';
  public errorCount: number = 0;

  constructor(private readonly http: HttpClient) {}

  private setTrainer(trainer: Trainer): void {
    setStorage<Trainer>(StorageKeys.TRAINER, trainer);
  }

  login(trainerName: string): Observable<any> {
    this.loading = true;
    console.log("login");

    const login$ = this.http.get<Trainer[]>(
      `${trainerAPI}/trainers?name=${trainerName}`
    );
    const register$ = this.http.post(`${trainerAPI}/trainers`, {
      name: trainerName,
      pokemon: [],
    });

    return login$.pipe(
      mergeMap((loginResponse: Trainer[]) => {
        const trainer = loginResponse.pop() as Trainer;
        if (!trainer) {
            
          return register$;
        } else {
            delete trainer.id
          return of(trainer);
        }
      }),
      tap((trainer: Trainer) => {
        this.setTrainer(trainer);
        this.getLoggedInName.emit(trainer.name)
      }),
      finalize(() => {
        this.loading = false;
      })
    )
  }
}
