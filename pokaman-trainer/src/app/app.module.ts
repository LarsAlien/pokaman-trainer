import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PokemonService } from './features/pokemon/services/pokemon.service';
import { AppRoutingModule } from './app-routing.module';
import { LoginContainer } from './features/login/containers/login.container';
import { PokemonDetailContainer } from './features/pokemon-detail/containers/pokemon-detail.container';
import { PokemonContainer } from './features/pokemon/containers/pokemon.container';
import { AppContainerComponent } from './shared/components/container/container.component';
import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TrainerContainer } from './features/trainer/containers/trainer.container';
import { NotFoundComponent } from './features/not-found/not-found.component';
import { LoginService } from './features/login/services/login.service';
import { LoginFormComponent } from './features/login/components/login-form/login-form.component';
import { PokemonListViewComponent } from './features/pokemon/components/pokemon-listview.component';
import { PokemonProfileHeader } from './features/pokemon-detail/components/pokemon-profile-header/pokemon-profile-header.component';

@NgModule({
  declarations: [
    //Components
    AppComponent,
    LoginFormComponent,
    NavbarComponent,
    NotFoundComponent,
    PokemonListViewComponent,
    PokemonProfileHeader,

    //Containers
    LoginContainer,
    PokemonDetailContainer,
    PokemonContainer,
    AppContainerComponent,
    TrainerContainer,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [PokemonService, LoginService],
  bootstrap: [AppComponent],
})
export class AppModule {}
