import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app.component";
import { LoginContainer } from "./features/login/containers/login.container";
import { NotFoundComponent } from "./features/not-found/not-found.component";
import { PokemonDetailContainer } from "./features/pokemon-detail/containers/pokemon-detail.container";
import { PokemonContainer } from "./features/pokemon/containers/pokemon.container";
import { TrainerContainer } from "./features/trainer/containers/trainer.container";
import { AuthGuard } from "./guards/auth.guard";

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/login'
    },
    {
        path: 'login',
        component: LoginContainer
    },
    {
        path: 'pokemon/:id',
        component: PokemonDetailContainer,
        canActivate: [ AuthGuard ]
    },
    {
        path: 'pokemon',
        component: PokemonContainer,
        canActivate: [ AuthGuard ]
    },
    {
        path: 'trainer',
        component: TrainerContainer,
        canActivate: [ AuthGuard ]
    },
    {
        path: '**',
        component: NotFoundComponent
    },
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
