export interface Pokemon {
  id?: number;
  image?: string;
  url: string;
  name: string;
  weight?: number;
  height?: number;
  types?: PokemonType[];
  stats?: PokemonStat[];
  sprites?: any[];
  owned?: boolean;
}

export interface PokemonSprite {
  back_shiny: string;
  front_shiny: string;
  other: PokemonSpriteOther;
}

export interface PokemonSpriteOther {
  dream_world: any;
  'official-artwork': PokemonSpriteOfficial;
}

export interface PokemonSpriteOfficial {
  front_default: string;
}

export interface PokemonType {
  slot: number;
  type: PokemonTypeType;
}

export interface PokemonTypeType {
  name: string;
  url: string;
}

export interface PokemonStat {
  base_stat: number;
  effort: number;
  stat: PokemonStatStat;
}

export interface PokemonStatStat {
  name: string;
  url: string;
}
