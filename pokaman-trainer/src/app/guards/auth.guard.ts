import { Observable } from 'rxjs';
import { Injectable } from "@angular/core";
import { AppRoutingModule } from '../app-routing.module'
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from "@angular/router";
import { SessionStorageService } from '../shared/services/session-storage.service';
import { Router } from '@angular/router';
import { AppRoutes } from 'src/app/enums/app-routes.enum';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(private session: SessionStorageService, private router: AppRoutingModule, private urlRouter: Router) {
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
            
            if ( this.session.active() ) {
                return true
            }
                this.urlRouter.navigateByUrl( AppRoutes.LOGIN )
                return false; 
        } 
    


}