export const environment = {
  production: false,
  pokeAPI: 'https://pokeapi.co/api/v2',
  trainerAPI: 'http://localhost:3000'
};